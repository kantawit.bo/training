package p2

func Prime() {
	num := 1937077
	isPrime := true

	for i := 2; i < num; i++ {
		if num%i == 0 {
			isPrime = false
			break
		}
	}

	if isPrime {
		println("Prime number")
	} else {
		println("Not prime number")
	}
}