package p6

import "fmt"

func Mah() {
	for i := 1; i <= 12; i++ {
		for j := 1; j <= 12; j++ {
			total := i * j
			fmt.Printf("%3d ",total)
		}
		fmt.Println()
	}
}