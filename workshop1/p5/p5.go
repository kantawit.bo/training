package p5

import "fmt"

func fibonacci(num int) int {
	if num <= 1 {
		return num
	}
	return fibonacci(num-1) + fibonacci(num-2)
}

func Show() {
	n := 10

	for i := 0; i < n; i++ {
		fmt.Printf("%d ", fibonacci(i))
	}
	
	fmt.Println()
}
