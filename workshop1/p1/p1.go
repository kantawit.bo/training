package p1

import "fmt"

func Agv() {
	valuse := [...]float64{1.5, 2.0, 3.5, 4.0, 5.5, 6.0, 7.5}
	sum := 0.0

	for i := 0; i < len(valuse); i++ {
		sum += valuse[i]
	}

	agv := sum/float64(len(valuse))
	fmt.Printf("%.2f \n",agv)

}
