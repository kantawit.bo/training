package models

import (
	"encoding/json"
	"time"

	"gorm.io/gorm"
)

type NewsArticle struct {
	Title       string
	Description string
	URL         string
	PublishedAt string
	Source      string
}

// NewsAPIResponse โครงสร้างข้อมูลการตอบกลับจาก News API
type NewsAPIResponse struct {
	Articles []NewsArticle `json:"articles"`
}

type Article1 struct {
	ID          uint      `json:"id"`
	SourceName  string    `json:"source_name"`
	Author      string    `json:"author"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	URL         string    `json:"url"`
	URLToImage  string    `json:"url_to_image"`
	PublishedAt time.Time `json:"published_at"`
	Content     string    `json:"content"`
}
type ArticleReponse struct {
	ID          uint      `json:"id"`
	SourceName  string    `json:"source_name"`
	Author      string    `json:"author"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	URL         string    `json:"url"`
	URLToImage  string    `json:"url_to_image"`
	PublishedAt time.Time `json:"published_at"`
	Content     string    `json:"content"`
}

type SourceDB struct {
	gorm.Model
	Name string
}

// data base
type News struct {
	gorm.Model
	Title       string `json:"title"`
	Description string `json:"description"`
	URL         string `json:"url"`
	PublishedAt string `json:"publishedAt"`
	Source      string `json:"source"`
}
type ApiResponse struct {
	Status       string    `json:"status"`
	TotalResults int       `json:"totalResults"`
	Articles     []Article `json:"articles"`
}

type Article struct {
	Source      Source    `json:"source"`
	Author      string    `json:"author"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	URL         string    `json:"url"`
	URLToImage  string    `json:"urlToImage"`
	PublishedAt time.Time `json:"publishedAt"`
	Content     string    `json:"content"`
}

type Source struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
type User struct {
	ID       uint
	CreateAt time.Time
	UpdateAt time.Time
	Name     string
	LastName string
}

type NewsRow struct {
	ID      uint
	RawData json.RawMessage
}

type ResponseError struct {
	Message    string `json:"message"`
	Status     string `json:"status"`
	StatusCode int    `json:"status_code"`
}

type ResponseData struct {
	Message    string      `json:"message"`
	Status     string      `json:"status"`
	StatusCode int         `json:"status_code"`
	Data       interface{} `json:"data"`
}
