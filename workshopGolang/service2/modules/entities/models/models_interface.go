package models

import (
	"encoding/json"

	"service2/modules/entities/events"
)

type NewsRepository interface {
	CreateNews(*Article1) error
	GetNews() ([]Article1, error)
	HasNews() (bool, error)
	NewsExists(newsID uint) bool
	FindNewsByID(ID uint) (*Article1, error)
}

type NewsUsecase interface {
	GetNews() ([]Article1, error)
	UserReadData(uint,uint) (json.RawMessage, error)
}

type EventHandlerProduce interface {
	Produce(event events.Event) error
}

type EventHandlerConsume interface {
	Handle(toppic string, eventByte []byte)
}

type UseCaseProducer interface {
	UserReaded(user *events.UserReadedEvent) error
}

type ConsumerUsecase interface {
	UserCreated(event events.UserCreatedEvent) error
	UserUpdated(event events.UserUpdatedEvent) error
	UserDeleted(event events.UserDeletedEvent) error
}

type UserRepository interface {
	CreateUser(string, string) (*User, error)
	UpdateUser(uint, string, string) (*User, error)
	DeleteUser(uint) (*string, error)
	UserExists(userID uint) bool
}
