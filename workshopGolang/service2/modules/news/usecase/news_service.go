package usecase

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2/log"

	"service2/modules/entities/events"
	"service2/modules/entities/models"
)

const (
	apiKey      = "5c288763c2334eea8be1958f46746074"
	apiURL      = "https://newsapi.org/v2/everything?q=apple&from=2023-12-02&to=2023-12-02&sortBy=popularity&apiKey=" + apiKey
	redisKey    = "service:GetNews"
	cacheExpiry = time.Minute * 1
)

type newsService struct {
	newsRepo        models.NewsRepository
	userRepo        models.UserRepository
	Redis           *redis.Client
	producerUsecase models.UseCaseProducer
}

func NewNewsService(
	newsRepo models.NewsRepository,
	userRepo models.UserRepository,
	Redis *redis.Client,
	producerUsecase models.UseCaseProducer,
) models.NewsUsecase {
	getDataAPI(newsRepo)
	return &newsService{newsRepo, userRepo, Redis, producerUsecase}
}

func (u *newsService) GetNews() (newss []models.Article1, err error) {
	key := "service:GetNews"

	//redis get
	if newssJson, err := u.Redis.Get(context.Background(), key).Result(); err == nil {
		if json.Unmarshal([]byte(newssJson), &newss) == nil {
			log.Debug("Read data from: redis")
			return newss, nil
		}
	}

	//repository
	log.Debug("Read data from: database")
	newsDB, err := u.newsRepo.GetNews()
	if err != nil {
		log.Error(err)
		return nil, errors.New("couldn't get drinks data")
	}

	for _, d := range newsDB {
		newss = append(newss, models.Article1{
			ID:          d.ID,
			SourceName: d.SourceName,
			Author: d.Author,
			Title:       d.Title,
			Description: d.Description,
			URL:         d.URL,
			PublishedAt: d.PublishedAt,
			URLToImage: d.URLToImage,
			Content: d.Content,
		})
	}

	if data, err := json.Marshal(newss); err == nil {
		u.Redis.Set(context.Background(), key, string(data), time.Minute*1)
	}

	return newss, nil
}

func newsToRawMessage(news models.Article1) (json.RawMessage, error) {
	newsDB, err := json.Marshal(news)
	if err != nil {
		return nil, err
	}

	rawMessage := json.RawMessage(newsDB)

	if !json.Valid(rawMessage) {
		return nil, errors.New("Not valid JSON")
	}

	return rawMessage, nil
}

func (u *newsService) UserReadData(userId, newsId uint) (json.RawMessage, error) {

	newsExist := u.newsRepo.NewsExists(newsId)
	if !newsExist {
		return nil, errors.New("news data not found")
	}

	userExist := u.userRepo.UserExists(userId)
	if !userExist {
		return nil, errors.New("user data not found")
	}

	newsDB, err := u.newsRepo.FindNewsByID(newsId)
	if err != nil {
		log.Error(err)
		return nil, errors.New("get news data faile")
	}

	rawMass, err := newsToRawMessage(*newsDB)
	if err != nil {
		return nil, err
	}
	log.Info(fmt.Sprintf("ui: %v,di: %v", userId, newsId))
	err = u.producerUsecase.UserReaded(&events.UserReadedEvent{
		UserId:      userId,
		NewsId:      newsId,
		NewsDetails: rawMass,
		TimeStamp:   time.Now(),
	})
	if err != nil {
		log.Error("con't not produce event UserReadData, error: ", err)
	}
	return rawMass, nil
}

func isAnyFieldNil(data map[string]interface{}) bool {
	for _, value := range data {
		if value == nil {
			return true
		}
	}
	return false
}
func getDataAPI(newsRepo models.NewsRepository) {
	// เรียกใช้ฟังก์ชัน getDataAPIJson เพื่อดึงข้อมูลจาก API
	newsData, err := getDataAPIJson()
	if err != nil {
		fmt.Println("Error getting data from API:", err)
		return
	}

	var result map[string]interface{}
	if err := json.Unmarshal([]byte(newsData), &result); err != nil {
		fmt.Println("Failed to parse JSON data:", err)
		return
	}

	// Extract articles from the result
	articles := result["articles"].([]interface{})

	// Iterate through articles and save them to the database
	for _, article := range articles {
		articleMap, ok := article.(map[string]interface{})
		if !ok {
			fmt.Println("Failed to convert article to map[string]interface{}")
			continue
		}

		var dbArticle models.Article1

		// Map JSON fields to the database model
		sourceData, ok := articleMap["source"].(map[string]interface{})
		if !ok || isAnyFieldNil(sourceData) || isAnyFieldNil(articleMap) {
			// fmt.Println("Skipping row with nil field.")
			continue
		}
		// Map JSON fields to the database model
		dbArticle.SourceName = articleMap["source"].(map[string]interface{})["name"].(string)
		dbArticle.Author = articleMap["author"].(string)
		dbArticle.Title = articleMap["title"].(string)
		dbArticle.Description = articleMap["description"].(string)
		dbArticle.URL = articleMap["url"].(string)
		dbArticle.URLToImage = articleMap["urlToImage"].(string)

		// Parse publishedAt string into time.Time
		publishedAt, err := time.Parse(time.RFC3339, articleMap["publishedAt"].(string))
		if err != nil {
			fmt.Println("Failed to parse publishedAt:", err)
			return
		}
		dbArticle.PublishedAt = publishedAt

		dbArticle.Content = articleMap["content"].(string)

		// fmt.Printf("Inserted Article: %+v\n", dbArticle)

		if err := newsRepo.CreateNews(&dbArticle); err != nil {
			fmt.Println("Error creating news:", err)
		} else {
			fmt.Printf("News %v created successfully\n", dbArticle)
		}

	}
}

func getDataAPIJson() ([]byte, error) {
	apiKey := "5c288763c2334eea8be1958f46746074"
	apiURL := "https://newsapi.org/v2/everything?q=apple&from=2023-12-03&to=2023-12-03&sortBy=popularity&apiKey=5c288763c2334eea8be1958f46746074"

	// ทำ HTTP GET request
	req, err := http.NewRequest("GET", apiURL, nil)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return nil, err
	}

	// เพิ่ม Header ที่ใส่ API Key
	req.Header.Add("x-api-key", apiKey)

	// สร้าง HTTP Client และส่ง request
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		fmt.Println("Error making HTTP request:", err)
		return nil, err
	}
	defer response.Body.Close()

	// เพิ่มส่วนการตรวจสอบ HTTP Status Code
	if response.StatusCode != http.StatusOK {
		fmt.Printf("Error: Unexpected status code %d\n", response.StatusCode)
		return nil, fmt.Errorf("Unexpected status code %d", response.StatusCode)
	}

	// อ่านข้อมูล response body
	body, err := io.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error reading response body:", err)
		return nil, err
	}
	return body, nil
}
