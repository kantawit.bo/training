package repository

import (
	"errors"
	"fmt"

	"gorm.io/gorm"

	"service2/modules/entities/models"
)

type newsRepositoryDB struct {
	db *gorm.DB
}

func NewNewsRepositoryDB(db *gorm.DB) newsRepositoryDB {
	db.AutoMigrate(models.Article1{})
	return newsRepositoryDB{db: db}
}

func (r newsRepositoryDB) CreateNews(news *models.Article1) error {
	// Create news record in the database
	if err := r.db.Create(news).Error; err != nil {
		return fmt.Errorf("failed to create news: %v", err)
	}

	return nil
}

func (r newsRepositoryDB) GetNews() ([]models.Article1, error) {
	var news []models.Article1
	err := r.db.Find(&news).Error
	if err != nil {
		return nil, errors.New("Get news not found")
	}
	
	return news, nil
}

func (r newsRepositoryDB) HasNews() (bool, error) {
	var count int64
	err := r.db.Model(&models.News{}).Count(&count).Error
	return count > 0, err
}

func (r newsRepositoryDB) NewsExists(newsID uint) bool {
	var news models.Article1
	result := r.db.First(&news, newsID)

	if result.Error == nil && result.RowsAffected > 0 {
		return true
	} else {
		return false
	}
}

func (r newsRepositoryDB) FindNewsByID(newsID uint) (*models.Article1, error) {
	var news models.Article1
	result := r.db.First(&news, newsID)

	if result.Error == nil && result.RowsAffected > 0 {
		return &news, nil
	} else {
		return nil, errors.New("find news by ID not found")
	}
}
