package controller

import (
	"strconv"

	"github.com/gofiber/fiber/v2"

	"service2/modules/entities/models"
)

type newsHandler struct {
	newsUsecase models.NewsUsecase
}

func NewNewsController(router fiber.Router, newsUsecase models.NewsUsecase) {
	controllers := &newsHandler{
		newsUsecase: newsUsecase,
	}

	router.Get("/news", controllers.getAllNewsData)
	router.Get("/news/:userId/:newsId", controllers.UserReaded)

}
func (h *newsHandler) getAllNewsData(c *fiber.Ctx) error {
	m, err := h.newsUsecase.GetNews()
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(
			models.ResponseError{
				Message:    err.Error(),
				Status:     fiber.ErrNotFound.Message,
				StatusCode: fiber.ErrNotFound.Code,
			},
		)
	}

	return c.Status(fiber.StatusOK).JSON(
		models.ResponseData{
			Message:    "Succeed",
			Status:     "OK",
			StatusCode: fiber.StatusOK,
			Data:       m,
		},
	)
}

func (h *newsHandler) UserReaded(c *fiber.Ctx) error {
	newsIdParam := c.Params("newsId")
	userIdParam := c.Params("userId")
	if userIdParam == "" || newsIdParam == "" {
		return c.Status(fiber.StatusBadRequest).JSON(
			models.ResponseError{
				Message:    "Require parameters",
				Status:     fiber.ErrBadRequest.Message,
				StatusCode: fiber.ErrBadRequest.Code,
			},
		)
	}
	newsid, err := strconv.ParseUint(newsIdParam, 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(
			models.ResponseError{
				Message:    "Invalid parameter type",
				Status:     fiber.ErrBadRequest.Message,
				StatusCode: fiber.ErrBadRequest.Code,
			},
		)
	}
	userId, err := strconv.ParseUint(userIdParam, 10, 64)
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(
			models.ResponseError{
				Message:    "Invalid parameter type",
				Status:     fiber.ErrBadRequest.Message,
				StatusCode: fiber.ErrBadRequest.Code,
			},
		)
	}
	res, err := h.newsUsecase.UserReadData(uint(userId), uint(newsid))
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(
			models.ResponseError{
				Message:    err.Error(),
				Status:     fiber.ErrNotFound.Message,
				StatusCode: fiber.ErrNotFound.Code,
			},
		)
	}
	return c.Status(fiber.StatusOK).JSON(
		models.ResponseData{
			Message:    "Succeed",
			Status:     "OK",
			StatusCode: fiber.StatusOK,
			Data:       res,
		},
	)
}
