# news Service Documentation

## Table of Contents

- [Drinks Service Documentation](#drinks-service-documentation)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Event topics](#event-topics)
    - [UserCreated](#usercreated)
    - [UserUpdated](#userupdated)
    - [UserDeleted](#userdeleted)
    - [UserReaded](#userreaded)
  - [Endpoints](#endpoints)
    - [GET /v1/newss](#get-v1news)
    - [GET /v1/news/:userId/:newsId](#get-v1newsuserIdnewsId)
  - [Status Codes](#status-codes)

## Introduction

"Welcome to the news Breed Information Service! This service utilizes text from Kafka and responds with news breed data, managing events for each topic, storing messages in a database, and fetching information from https://api.thenewsapi.com/v1/breeds."

## Event topics

### UserCreated

When the user makes an event `UserCreated`, it will create a user in the database ifdoe.

**Message**:

```json
{
  "id": 1,
  "name": "John",
  "lastname": "Doe",
  "time_stamp": "Time Stamp"
}
```

### UserUpdated

When the user makes an event `UserUpdated`, it will update user data

**Message**:

```json
{
  "id": 1,
  "name": "John",
  "lastname": "Doe",
  "time_stamp": "Time Stamp"
}
```

### UserDeleted

Deleted user from data base.

**Message**:

```json
{
  "id": 1
}
```

### UserReaded

When the user receives news breed information with `newsId` it will send message to topic `UserReaded`

**Message**:

```json
{
  "user_id": "uint",
  "item_id": "uint",
  "item_details": {},
  "time_stamp": "time Stamp"
}
```

## Endpoints

### GET /v1/news

get news breeds.

**Response**:

- Code: `200`

```json
{
    "message": "Succeed",
    "status": "OK",
    "status_code": 200,
    "data": [
        {
            "id": 1,
            "source_name": "Wired",
            "author": "David Nield",
            "title": "How to Not Get Hacked by a QR Code",
            "description": "QR codes can be convenient—but they can also be exploited by malicious actors. Here’s how to protect yourself.",
            "url": "https://www.wired.com/story/how-to-qr-code-hacks-avoid/",
            "url_to_image": "https://media.wired.com/photos/656a1ad6f8b0b4ca60e7a974/191:100/w_1280,c_limit/How-not-to-get-hacked-by-a-QR-code-Gear.jpg",
            "published_at": "2023-12-03T12:00:00Z",
            "content": "And you dont need anything special to create a QR code. The tools are widely available and straightforward to use, and putting together a QR code of your own isnt much more difficult than scanning on… [+2507 chars]"
        },{.........}
        ]
}
```

### GET /v1/news/:userId/:newsId (UserReaded)

User get news breed information with news id. and sent data to topic `UserReaded`

**Parameters**:

- `newsId`: (require) type uint.
- `userId` : (require) type uint.

**Response**:

```json
{
  "message": "Succeed",
    "status": "OK",
    "status_code": 200,
    "data":  {
            "id": 1,
            "source_name": "Wired",
            "author": "David Nield",
            "title": "How to Not Get Hacked by a QR Code",
            "description": "QR codes can be convenient—but they can also be exploited by malicious actors. Here’s how to protect yourself.",
            "url": "https://www.wired.com/story/how-to-qr-code-hacks-avoid/",
            "url_to_image": "https://media.wired.com/photos/656a1ad6f8b0b4ca60e7a974/191:100/w_1280,c_limit/How-not-to-get-hacked-by-a-QR-code-Gear.jpg",
            "published_at": "2023-12-03T12:00:00Z",
            "content": "And you dont need anything special to create a QR code. The tools are widely available and straightforward to use, and putting together a QR code of your own isnt much more difficult than scanning on… [+2507 chars]"
        }
}
```

**Response Error**:

```json
{
  "message": "No data found",
  "status": "Not Found",
  "status_code": 404
}
```

```json
{
  "message": "Invalid parameter type",
  "status": "Bad Request",
  "status_code": 400
}
```

## Status Codes

<ul>
  <li>200 : OK. Request was successful.</li>
  <li>201 : Created. Resource was successfully created.</li>
  <li>202 : Accepted response status code indicates that the request has been accepted for processing, but the processing has not been completed.</li>
  <li>400 : Bad request. The request was invalid or cannot be served.</li>
 authentication credentials.</li>
  <li>404 : No data found</li>
</ul>
