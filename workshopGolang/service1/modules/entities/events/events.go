package events

import (
	"encoding/json"
	"time"
)

type Event interface {
	String() string
}

var SubscribedTopics = []string{
	UserReadedEvent{}.String(),
}

type UserCreatedEvent struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	LastName     string    `json:"lastName"`
	TimeStamp time.Time `json:"timeStamp"`
}

type UserUpdatedEvent struct {
	ID        uint      `json:"id"`
	Name      string    `json:"name"`
	LastName     string    `json:"lastName"`
	TimeStamp time.Time `json:"timeStamp"`
}

type UserDeletedEvent struct {
	ID uint `json:"id"`
}

type UserReadedEvent struct {
	UserId     uint            `json:"userId"`
	NewsId      uint            `json:"newsId"`
	NewsDetails json.RawMessage `json:"itemDetails"`
	TimeStamp  time.Time       `json:"timeStamp"`
}

func (UserCreatedEvent) String() string {
	return "UserCreated"
}
func (UserUpdatedEvent) String() string {
	return "UserUpdated"
}
func (UserReadedEvent) String() string {
	return "UserReaded"
}
func (UserDeletedEvent) String() string {
	return "UserDeleted"
}
