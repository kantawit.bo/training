package models

import (
	"encoding/json"
	"time"
)

type UserResponse struct {
	Id    uint   `json:"id"`
	Name  string `json:"name"`
	LastName string `json:"LastName"`
}

type UserRequest struct {
	Id    uint   `json:"id"`
	Name  string `json:"name"`
	LastName string `json:"LastName"`
}

// db
type User struct {
	ID       uint   `gorm:"primaryKey;autoIncrement;column:id"`
	Name  string `gorm:"not null"`
	LastName string `gorm:"unique;not null"`
}

type UserReadNews struct {
	ID         uint `gorm:"primaryKey;autoIncrement"`
	CreateAt   time.Time
	UserID     uint
	NewsID      uint
	NewsDetails json.RawMessage
}


type ResponseError struct {
	Message    string `json:"message"`
	Status     string `json:"status"`
	StatusCode int    `json:"status_code"`
}

type ResponseData struct {
	Message    string      `json:"message"`
	Status     string      `json:"status"`
	StatusCode int         `json:"status_code"`
	Data       interface{} `json:"data"`
}