package usecase

import (
	"github.com/gofiber/fiber/v2/log"

	"service1/modules/entities/events"
	"service1/modules/entities/models"
)

type ConsumerService interface {
	UserReaded(event events.UserReadedEvent) error
}

type consumerService struct {
	repo models.ConsumerRepository
}

func NewConsumerUsecase(comsumerRepo models.ConsumerRepository) ConsumerService {
	return &consumerService{repo: comsumerRepo}
}

func (u *consumerService) UserReaded(event events.UserReadedEvent) error {

	err := u.repo.CreateUserReadedNews(&models.UserReadNews{
		CreateAt:   event.TimeStamp,
		UserID:     event.UserId,
		NewsID:      event.NewsId,
		NewsDetails: event.NewsDetails,
	})
	if err != nil {
		log.Error(err)
		return err
	}
	return nil

}
